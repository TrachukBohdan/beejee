<?php

    namespace app\models;

    use core\DB;

    /**
     * Class Task - модель для роботи із тасками
     * @package app\models
     */
    abstract class Task
    {
        /**
         * створення таска
         * @param $data
         */
        public static function insert($data)
        {
            $db = DB::get();
            $sql = "
              INSERT INTO `tasks`
              SET
                `name` = :name,
                `email` = :email,
                `description` = :description,
                `poster` = :poster,
                `created_at` = :created_at,
                `updated_at` = :updated_at
            ";

            $data['created_at'] = time();
            $data['updated_at'] = time();

            $stmt = $db->prepare($sql);
            $stmt->bindParam(':name', $data['name'],  \PDO::PARAM_STR);
            $stmt->bindParam(':email', $data['email'],  \PDO::PARAM_STR);
            $stmt->bindParam(':description', $data['description'],  \PDO::PARAM_STR);
            $stmt->bindParam(':poster', $data['poster'],  \PDO::PARAM_STR);
            $stmt->bindParam(':created_at', $data['created_at'],  \PDO::PARAM_INT);
            $stmt->bindParam(':updated_at', $data['updated_at'],  \PDO::PARAM_INT);
            $stmt->execute();
        }

        public static function find($id)
        {
            $db = DB::get();
            $sql = "
              SELECT * FROM `tasks` WHERE `id` = :id
            ";

            $stmt = $db->prepare($sql);
            $stmt->bindParam(':id', $id,  \PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch();
        }

        /**
         * Список всіх тасків
         * @param array $sort - фільтр
         * @return array
         */
        public static function all($sort = [])
        {
            $filter = '';

            if(isset($sort['order_by']))
            {
                $filter .= " ORDER BY `{$sort['order_by']}`";

                if(isset($sort['order']))
                {
                    $filter .= " {$sort['order']}";
                }
                else
                {
                    $filter .= " ASC";
                }
            }

            $db = DB::get();
            $tasks = $db->query("SELECT * FROM `tasks`" . $filter);
            return $tasks->fetchAll();
        }

        /**
         * Оновлення таска по id
         * @param $taskId
         * @param $data
         */
        public static function update($taskId, $data)
        {
            $db = DB::get();
            $sql = "
              UPDATE `tasks`
              SET
                `status` = :status,
                `description` = :description,
                `updated_at` = :updated_at
              WHERE `id` = :id
            ";

            $data['updated_at'] = time();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':description', $data['description'],  \PDO::PARAM_STR);
            $stmt->bindParam(':status', $data['status'],  \PDO::PARAM_INT);
            $stmt->bindParam(':id', $taskId,  \PDO::PARAM_INT);
            $stmt->bindParam(':updated_at', $data['updated_at'],  \PDO::PARAM_INT);
            $stmt->execute();
        }

    }