<?php

    /**
     * Роутінг
     */
    return [

        //---------------------------------  TaskController

        '/'                     => 'TaskController@showIndex',
        '/task/add'             => 'TaskController@showAddTask',
        '/task/edit/{id}'       => 'TaskController@showEditTask',
        '/task/store'           => 'TaskController@storeTask',
        '/task/update/{id}'     => 'TaskController@updateTask',

        //---------------------------------  AuthController

        '/login'    =>  'AuthController@login',
        '/logout'   =>  'AuthController@logout'
    ];