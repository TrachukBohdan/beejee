<?php use core\TPL; ?>
<?php TPL::render('_parts/header') ?>

<h3>Errors</h3>
<hr>
<div class="alert alert-warning">
    <ul>
        <?php foreach($errors as $err): ?>
            <li> <?=$err?> </li>
        <?php endforeach ?>
    </ul>
</div>

<?php TPL::render('_parts/footer') ?>

