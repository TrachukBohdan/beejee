<nav class="navbar">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">BeeJee Tasks</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

            <?php if(!\app\helpers\Auth::check()): ?>
                <form method="post" action="/login" class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" name="login" placeholder="Login" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="pass" placeholder="Password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Sign in</button>
                </form>
            <?php else: ?>
                <div class="navbar-form navbar-right">
                    <b>Hi admin!</b>
                    <a href="/logout" class="btn btn-success">Loutout</a>
                </div>
            <?php endif ?>

        </div>
    </div>
</nav>

