<?php use core\TPL; ?>
<?php TPL::render('_parts/header') ?>

<h1>Edit task</h1>
<hr />

<form  method="post" action="/task/update/<?=$task['id']?>">

    <div class="checkbox">
        <label><input name="status" value="1" type="checkbox" <?=( $task['status'] == 1 ? 'checked' : '' )?> > Done task</label>
    </div>

    <div class="form-group">
        <label for="pwd">Task description:</label>
        <textarea name="description" class="form-control" rows="10"><?=$task['description']?></textarea>
    </div>

    <button type="submit" class="btn btn-warning">Edit task</button>
</form>

<hr>

<?php TPL::render('_parts/footer') ?>



