<?php
    use core\TPL;
    use app\helpers\Auth;
?>
<?php TPL::render('_parts/header') ?>

    <div class="row">
        <div class="col-md-12">
            <a href="/task/add" class="btn btn-success">Add task</a>
        </div>
    </div>

    <hr />
        <div class="row">

            <form action="/" method="post">

                <div class="col-md-2">
                    <div class="form-group">
                        <select name="order_by" class="form-control" id="sel1">
                            <?php foreach(['email', 'name', 'status'] as $selectName): ?>
                                <option value="<?=$selectName?>" <?=( $_POST['order_by'] == $selectName ? 'selected' : '')?> >
                                    <?=$selectName?>
                                </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <select name="order" class="form-control" id="sel1">
                            <?php foreach(['ascending' => 'asc', 'descending' => 'desc'] as $selectName => $selectVal): ?>
                                <option value="<?=$selectVal?>" <?=( $_POST['order'] == $selectVal ? 'selected' : '')?> >
                                    <?=$selectName?>
                                </option>
                            <?php endforeach ?>

                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="sort" />
                    </div>
                </div>

            </form>

        </div>

        <?php foreach(array_chunk($tasks,3) as $tasksRow): ?>
            <div class="row">

                <?php foreach($tasksRow as $task): ?>
                    <div class="col-md-4">
                        <h2><?=$task['name']?></h2>
                        <img src="/public/uploads/posters/<?=$task['poster']?>">

                        <?php if($task['status'] != 0): ?>
                            <span class="label label-success">done</span>
                        <?php endif ?>

                        <p><?=$task['email']?></p>
                        <p><?=$task['description']?></p>

                        <?php if(Auth::userCan('editTask')): ?>
                            <p><a class="btn btn-warning" href="/task/edit/<?=$task['id']?>" role="button">Edit task</a></p>
                        <?php endif ?>
                    </div>
                <?php endforeach ?>

            </div>
        <?php endforeach ?>


    <hr>

<?php TPL::render('_parts/footer') ?>


