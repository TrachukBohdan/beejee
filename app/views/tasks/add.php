<?php use core\TPL; ?>
<?php TPL::render('_parts/header') ?>

    <h1>Add task</h1>
    <hr />

    <form method="post" enctype="multipart/form-data" action="/task/store">

        <div class="form-group">
            <label for="name">User:</label>
            <input type="text" class="form-control" name="name" id="name">
        </div>

        <div class="form-group">
            <label for="email">Email address:</label>
            <input type="email" name="email" class="form-control" id="email">
        </div>

        <div class="form-group">
            <label for="poster">Photo:</label>
            <input type="file" name="poster" class="form-control" id="photo">
        </div>

        <div class="form-group">
            <label for="description">Task description:</label>
            <textarea name="description" class="form-control" rows="10"></textarea>
        </div>

        <button type="button" id="preview-button" class="btn btn-success" data-toggle="modal" data-target="#previewModal">Preview task</button>
        <button type="submit" class="btn btn-warning">Add task</button>
    </form>

    <hr>

    <!-- Modal -->
    <div id="previewModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Task</h4>
                </div>
                <div class="modal-body">
                    <b>username: </b> <span id="preview-username"></span> <br />
                    <b>email: </b> <span id="preview-email"></span> <br />
                    <b>description: </b> <span id="preview-description"></span> <br />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function($){
            var previewButton = $('#preview-button');

            previewButton.click(function(){
                $('#preview-username').html( $('input[name=name]').val() );
                $('#preview-email').html( $('input[name=email]').val() );
                $('#preview-description').html( $('textarea[name=description]').val() );
            });

        });
    </script>

<?php TPL::render('_parts/footer') ?>



