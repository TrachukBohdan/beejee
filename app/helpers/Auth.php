<?php

namespace app\helpers;

/**
 * Class Auth - хелпер для аторизації та перевірки прав користувача
 * @package app\helpers
 */
abstract class Auth
{
    /**
     * True якщо користувач авторизований
     * @return bool
     */
    public static function check()
    {
        if(isset($_SESSION['auth']) and $_SESSION['auth'] == true)
        {
            return true;
        }

        return false;
    }

    /**
     * Вхід на сайт
     * @param $login
     * @param $pass
     */
    public static function login($login, $pass)
    {
        if($login == 'admin' and $pass == '123')
        {
            $_SESSION['auth'] = true;
        }

    }

    /**
     * Вихід із сайту
     */
    public static function logout()
    {
        $_SESSION['auth'] = false;
    }

    /**
     * Перевірка прав користувача
     * @param $action
     * @return bool
     */
    public static function userCan($action)
    {
        $allowActions = [
            'editTask' // редагування задачі
        ];
        return in_array($action, $allowActions) and self::check();
    }
}