<?php

    namespace app\helpers;

    /**
     * Хелпер для зміни розміру зображення
     * Class ResizeImg
     * @package app\helpers
     */
    abstract class ResizeImg
    {
        /**
         * Змінює розмір зображення
         * @param $filePath - шлях до файлу
         * @param $width - бажана ширина
         * @param $height - бажана висота
         */
        public static function resize($filePath, $width, $height)
        {
            list($imgWidth, $imgHeight) = getimagesize($filePath);

            if($imgWidth > $imgHeight)
            {
                $newImgWidth = $width;
                $newImgHeight = ceil( $height / ($imgWidth / $imgHeight));
                $dstX = 0;
                $dstY = ($height - $newImgHeight) / 2;
            }
            else
            {
                $newImgHeight = $height;
                $newImgWidth = ceil( $width / ($imgHeight / $imgWidth) );
                $dstX = ($width - $newImgWidth) / 2;
                $dstY = 0;
            }

            $src = imagecreatefromjpeg($filePath);
            $dst = imagecreatetruecolor($width, $height);

            imagecopyresampled($dst, $src, $dstX, $dstY, 0, 0, $newImgWidth, $newImgHeight, $imgWidth, $imgHeight);
            imagejpeg($dst, $filePath);

        }
    }