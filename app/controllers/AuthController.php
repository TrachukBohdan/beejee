<?php

    namespace app\controllers;

    use app\helpers\Auth;

    /**
     * Class AuthController
     * @package app\controllers
     */
    class AuthController extends CoreController
    {
        /**
         * Вхід на сайт
         */
        public function login()
        {
            Auth::login($_POST['login'], $_POST['pass']);
            header('location: /');
        }

        /**
         * вихдід
         */
        public function logout()
        {
            Auth::logout();
            header('location: /');
        }
    }