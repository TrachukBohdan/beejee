<?php

    namespace app\controllers;
    use app\helpers\Auth;
    use app\helpers\ResizeImg;
    use app\models\Task;
    use core\TPL;

    /**
     * Class TaskController
     * @package app\controllers
     */
    class TaskController extends CoreController
    {
        private $errors = [];

        private $posterExtensions = ['jpg', 'jpeg', 'gif'];
        private $posterStorePath =  __DIR__ .'/../../public/uploads/posters/';
        private $posterSize = ['width' => 320, 'height' => 240];

        /**
         * Головна сторінка
         */
        public function showIndex()
        {

            $filter = [
                'order_by'  => 'id',
                'order'     => 'asc'
            ];

            if( strtoupper($_SERVER['REQUEST_METHOD']) == 'POST')
            {


                $allowedOrderBy = ['name', 'email', 'status'];
                $allowedOrder = ['asc', 'desc'];

                if(in_array($_POST['order_by'], $allowedOrderBy))
                {
                    $filter['order_by'] = $_POST['order_by'];
                }

                if(in_array($_POST['order'], $allowedOrder))
                {
                    $filter['order'] = $_POST['order'];
                }
            }

            $tasks = Task::all($filter);

            TPL::render('tasks/list', [
                'tasks' => $tasks
            ]);
        }


        /**
         * Форма додавання таску
         */
        public function showAddTask()
        {
            TPL::render('tasks/add');
        }

        /**
         * Форма редагування таску
         */
        public function showEditTask($taskId)
        {
            $task = Task::find($taskId);

            TPL::render('tasks/edit', [
                'task' => $task
            ]);
        }

        public function updateTask($taskId)
        {
            if( !Auth::userCan('editTask') )
            {
                header('location: /');
                exit;
            }

            $data = $_POST;
            $this->validateUpdate($data);
            Task::update($taskId, $data);
            header('location: /');
        }

        private function validateUpdate($data)
        {
            if(!preg_match('/^.{3,}$/i', $data['description']))
            {
                $this->errors['description'] = 'fill description';
            }

            if(!in_array($data['status'], [1,2]))
            {
                $this->errors['status'] = 'wrong status';
            }
        }


        /**
         * Збереження таска
         */
        public function storeTask()
        {
            $data = $_POST;
            $this->validateAdd($data);

            if(count($this->errors) == 0)
            {
                $posterInfo = pathinfo($_FILES['poster']['name']);
                $posterSize = getimagesize($_FILES['poster']['tmp_name']);

                $data['poster'] = uniqid() .'.'. strtolower( $posterInfo['extension'] );

                move_uploaded_file(
                    $_FILES['poster']['tmp_name'],
                    $this->posterStorePath . $data['poster']
                );

                if( $posterSize[0]  > $this->posterSize['width']  or
                    $posterSize[1] > $this->posterSize['height']
                )
                {
                    ResizeImg::resize(
                        $this->posterStorePath . $data['poster'],
                        $this->posterSize['width'],
                        $this->posterSize['height']
                    );
                }

                Task::insert($data);
                header('location: /');
            }

            TPL::render('errors', ['errors' => $this->errors]);
        }

        /**
         * Валідація таска
         * @param $data
         */
        public function validateAdd($data)
        {
            if(!preg_match('/^.{3,40}$/i', $data['name']))
            {
                $this->errors['name'] = 'fill name';
            }

            if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL))
            {
                $this->errors['email'] = 'fill email';
            }

            if(!preg_match('/^.{3,}$/i', $data['description']))
            {
                $this->errors['description'] = 'fill description';
            }

            if(UPLOAD_ERR_OK != $_FILES['poster']['error'])
            {
                $this->errors['file'] = 'upload file';
            }

            $posterExtension = strtolower(pathinfo($_FILES['poster']['name'])['extension']);

            if(!in_array($posterExtension, $this->posterExtensions))
            {

                $this->errors['poster'] = 'poster file format: ' . $posterExtension;
            }
        }
    }