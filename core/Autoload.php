<?php

    namespace core;

    /**
     * Class Autoload - Автозавантаження класів
     * @package core
     */
    abstract class Autoload
    {
        public static function load()
        {
            spl_autoload_register(function($className){
                $classPath = str_replace('\\', '/', $className);
                include_once "$classPath.php";
            });
        }
    }