<?php

    namespace core;

    /**
     * Class TPL - базова обгортка до шаблона
     * @package core
     */
    abstract class TPL
    {

        public static $viewPath = 'app/views/';

        /**
         * Рендер шаблона
         * @param $view
         */
        public static function render($view, $data = [])
        {
            extract($data);
            $incPath =  self::$viewPath . $view . '.php';
            include_once $incPath;
        }
    }