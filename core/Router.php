<?php

    namespace core;

    abstract class Router
    {
        /**
         * неймспейс контроллерів
         * @var string
         */
        private static $controllerPath = 'app\controllers\\';

        /**
         * Шукає співпадіння із файлом routes.php і запускає відповідний метод
         * @throws Exception
         */
        public static function route()
        {
            // роути (масив)
            $routes = include_once 'app/routes.php';
            $query = $_SERVER['REQUEST_URI'];

            if( in_array( $query, array_keys($routes) ) )
            {
                /**
                 * Якщо є пряме співпадіння із роутом
                 * то просто вибираємо функцію із масиву
                 */

                self::runMethod($routes[$query]);
            }
            else
            {
                /**
                 * Якщо немає прямого співпадіння то шукаємо
                 * роути із параметрами
                 */

                foreach($routes as $route => $method)
                {
                    // заміняємо шаблон із фігурними дужками на регулярний вираз
                    $routePattern ='!^'. preg_replace("#{(.*?)}#", "(.*?)", $route) .'$!';
                    preg_match_all($routePattern, $query, $args, PREG_SET_ORDER);

                    if( count($args) > 0 )
                    {
                        $args = $args[0];
                        array_shift($args);
                        self::runMethod($method, $args);
                        break;
                    }
                }
            }
        }

        /**
         * Запускає метод класа контроллера
         * @param $method
         * @param array $args
         * @throws Exception
         */
        private static function runMethod( $method, $args = [] )
        {
            list($routedClass, $routedMethod) = explode('@', $method);

            $routedClass = self::$controllerPath . $routedClass;
            $routedObject = new $routedClass();

            if( method_exists($routedObject, $routedMethod) )
            {
                call_user_func_array( [$routedObject, $routedMethod], $args);
            }
            else
            {
                throw new Exception("Router: Метод '$routedMethod' не існує в класі '$routedClass'.");
            }
        }

    }