<?php

    namespace core;

    /**
     * Class Config
     * витягування конфігів із файлу
     * @package core
     */
    abstract class Config
    {
        private static $configs = null;

        public static function get()
        {
            if(is_null(self::$configs))
            {
                self::$configs = (object) require_once __DIR__ . '/../config/config.php';
            }

            return self::$configs;
        }
    }