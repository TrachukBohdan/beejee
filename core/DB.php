<?php
    namespace core;

    /**
     * Class DB - зєднання з базою
     * @package core
     */
    class DB
    {
        private static $inst = null;

        public static function get()
        {
            if( is_null( self::$inst ) )
            {
                $dbConfigs = Config::get()->db;
                $dsn = "mysql:host=". $dbConfigs['host'] .";dbname=". $dbConfigs['db'];

                try {

                    self::$inst = new \PDO (
                        $dsn,
                        $dbConfigs['user'],
                        $dbConfigs['pass'],
                        array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8 COLLATE utf8_unicode_ci")
                    );

                    self::$inst->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                    self::$inst->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
                    self::$inst->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);

                } catch(\Exception $e){
                    die ($e->getMessage());
                }
            }

            return self::$inst;
        }
    }