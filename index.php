<?php

    header('content-type: text/html; charset=utf8');
    session_start();

    include_once __DIR__ . '/core/Autoload.php';

    \core\Autoload::load();
    \core\Router::route();
