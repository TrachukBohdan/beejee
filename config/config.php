<?php

    /**
     *  Збирає налаштування сайту в один масив
     */

    $configFiles = [
        'common',
        'db'
    ];

    $config = [];

    foreach($configFiles as $fileConfig)
    {
        $config[$fileConfig] = include_once "$fileConfig.php";
    }

    return $config;